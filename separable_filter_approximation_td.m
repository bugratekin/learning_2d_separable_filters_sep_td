function [] = separable_filter_approximation_td()
%  Approximates a 2D filter bank with a separable filter bank.
%
%  Synopsis:
%     separable_filter_approximation_td()
%
%  author: Bugra Tekin, CVLab EPFL
%  e-mail: bugra <dot> tekin <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/
%  date: October 2013
%  last revision: 23 December 2013

%% Initialization

tic;

p = get_config; % <--- MODIFY HERE the algorithm's parameters

% Load original filterbank
[p,original_filters] = load_original_fb(p);

%% CP decomposition

% Apply CP-OPT
[P] = cp_opt( tensor( original_filters ), p.rank);

% Find the components of the Kruskal form
A = P.U{1};
B = P.U{2};
C = P.U{3};


%% Compute separable filters, normalize

sep = zeros(p.filters_size,p.filters_size,p.rank);
normSep = zeros(p.rank);
M = zeros(p.filters_size^2, p.rank);
for j = 1 : p.rank
    sep(:,:,j) = A(:,j)*B(:,j)';
    normSep(j) = norm(sep(:,:,j));
    sep(:,:,j) = sep(:,:,j)/normSep(j);
end


%% Recompose the tensor

% Initialize the reconstructed filter banks
coefs = zeros(p.rank,p.filters_no);
recomp = zeros(p.filters_size, p.filters_size, p.filters_no);

% Recompose the tensor
for i=1:p.filters_no
    sum_recomp = 0;
    for j=1:p.rank
        sum_recomp = sum_recomp + P.lambda(j)*normSep(j)*sep(:,:,j)*C(i,j);
        coefs(j,i) = P.lambda(j)*C(i,j)*normSep(j);
    end
    recomp(:,:,i) = sum_recomp;
end
recomp = double(recomp);
coefs = double(coefs);

cpuExecutionTime = toc;

%% Calculate the error of the approximation of the filter bank with separable filters

sum_err_recomp = 0;
err_recomp = zeros(p.filters_no,1);
for i=1:p.filters_no
    err_recomp(i) = norm(double(original_filters(:,:,i))-double(recomp(:,:,i)))^2/(p.filters_size*p.filters_size);
    sum_err_recomp = sum_err_recomp + err_recomp(i);
end

% Find the average error 
avg_err_recomp = sum_err_recomp / p.filters_no;

%% Save the reconstructed filters and reconstruction coefficients in the text and image format

% Initialize the filter bank
rec_fb = zeros(p.filters_size*p.filters_size,p.filters_no);

fd = fopen(sprintf('%s/reconstructed_rank%03d.txt',p.resulting_filters_directory, p.rank), 'wt');
for i=1:p.filters_no
    filter = recomp(:,:,i);
    rec_fb(:,i) = filter(:);
    for r = 1 : p.filters_size
        fprintf(fd, '%f ', filter(r, :));
        fprintf(fd, '\n');
    end 
end
fclose(fd);

% Save the reconstructed filters in .nrrd format
nrrdSave(sprintf('%s/reconstructed_rank%03d.nrrd',p.resulting_filters_directory, p.rank), reshape_fb_as_img(p, rec_fb,size(rec_fb,2))');

% Save the reconstruction coefficients in .txt format
save(sprintf('%s/coefs_rank%03d.txt',p.resulting_filters_directory, p.rank),'coefs','-ascii')


%% Save the separable filters in the text and image format

fd = fopen(sprintf('%s/separable_rank%03d.txt',p.resulting_filters_directory, p.rank), 'wt');
for j=1:p.rank
    filter = sep(:,:,j);
    M(:, j) = filter(:);
    normM = norm(M(:,j)); 
    M(:,j) = M(:,j)/normM;
    for i = 1 : p.filters_size
        fprintf(fd, '%f ', filter(i, :));
        fprintf(fd, '\n');
    end 
end
fclose(fd);

% Save the separable filters in nrrd format
nrrdSave(sprintf('%s/separable_rank%03d.nrrd',p.resulting_filters_directory, p.rank), reshape_fb_as_img(p, M, size(M,2))');

%% Output error and execution time

fd = fopen(sprintf('%s/stats_rank%03d.txt',p.resulting_filters_directory, p.rank), 'wt');
fprintf(fd, 'The average reconstruction error for rank %d is %f\n',p.rank, avg_err_recomp);
fprintf(fd ,'Execution time: %f sec\n', cpuExecutionTime);
fclose(fd);
fprintf('The average reconstruction error for rank %d is %f\n',p.rank, avg_err_recomp);
fprintf('Execution time: %f sec\n', cpuExecutionTime);

end
