function [p,original_filters] = load_original_fb(p)
%  load_original_fb  Loads the original filter bank (the one we want
%                    to approximate).
%
%  Synopsis:
%     [p,original_filters] = load_original_fb(p)
%
%  Input:
%     p = simulation's parameters
%  Output:
%     p                = simulation's parameters
%     original_filters = loaded filter bank
%
%  author: Roberto Rigamonti, CVLab EPFL
%  e-mail: roberto <dot> rigamonti <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~rigamont
%  date: April 2013
%  last revision: 10 April 2013

tmp_fb = load(p.original_filters_txt);
filters_size = size(tmp_fb,2);
filters_no = size(tmp_fb,1)/filters_size;

original_filters = zeros(filters_size,filters_size,filters_no);

for i_filter = 1:filters_no
    original_filters(:,:,i_filter) = tmp_fb((i_filter-1)*filters_size+1:i_filter*filters_size,:);
end

p.filters_no = filters_no;
p.filters_size = filters_size;

end

