function [img] = reshape_fb_as_img(p, fb, filters_no)
%  reshape_result_fb_as_img  Reshape the separable filter bank as
%                            an image
%
%  Synopsis:
%     [img] = reshape_separable_fb_as_img(p,M,filters_no)
%
%  Input:
%     filters_size = size of the filters in the filter bank
%     filters_no   = number of filters in the filter bank
%     fb           = filter bank

%  author: Roberto Rigamonti, CVLab EPFL
%  e-mail: roberto <dot> rigamonti <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~rigamont
%  date: April 2013
%  last revision: 10 April 2013

n_blocks = filters_no;
n_block_rows = ceil(sqrt(n_blocks));
n_block_cols = ceil(sqrt(n_blocks));

img = 255*ones(n_block_rows*p.filters_size*p.pixS + (n_block_rows-1)*p.vSpace, ...
               n_block_cols*p.filters_size*p.pixS + (n_block_cols-1)*p.hSpace);

rowN = 1;
colN = 1;
for f = 1 : filters_no
    filter = reshape(fb(:, f), p.filters_size, p.filters_size);
    filter = magnify_matrix(filter, p.pixS);
    filter = convert_img_visualization(filter);
    
    if(colN == n_block_cols+1)
        rowN = rowN+1;
        colN = 1;
    end
    
    img((rowN-1)*p.filters_size*p.pixS + (rowN-1)*p.vSpace + 1 : rowN*p.filters_size*p.pixS + (rowN-1)*p.vSpace, ...
        (colN-1)*p.filters_size*p.pixS + (colN-1)*p.hSpace + 1 : colN*p.filters_size*p.pixS + (colN-1)*p.hSpace) = filter;
    colN = colN+1;
end
