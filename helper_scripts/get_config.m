function [p] = get_config()
%  get_config  Setup file for the approximation algorithm.
%
%  Synopsis:
%     get_config()
%
%  author: Bugra Tekin, CVLab EPFL
%  e-mail: bugra <dot> tekin <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/
%  date: October 2013
%  last revision: 23 December 2013

% Filter bank path
p.original_filters_txt = 'data/learned_121_DRIVE.txt';

% Create folder to store the results
p.resulting_filters_directory = 'results';
[status, message, messageid] = rmdir(p.resulting_filters_directory, 's'); %#ok<*NASGU,*ASGLU>
[status, message, messageid] = mkdir(p.resulting_filters_directory); %#ok<*ASGLU,*NASGU>

% Number of desired filters in the separable filter bank
p.rank = 25;

% Visualization parameters
p.vSpace = 4;
p.hSpace = 4;
p.pixS = 2;

end

